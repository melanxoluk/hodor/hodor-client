import React, { useState } from "react"
import useFetchHodor, { usePostHodor } from "./fetchHodor"
import { Button, Card, ControlGroup, FormGroup, H5, InputGroup, NonIdealState } from "@blueprintjs/core"

interface Client {
  id: number
  appId: number
  type: string
  uuid: string
}

interface App {
  id: number
  name: string
  uuid: string
}

interface AppResp {
  app: App
  clients: Client[]
}

interface NewApp {
  name: string
}

const Apps = () => {
  const [, setCreateApp] = usePostHodor<NewApp, NewApp>("/apps")
  const [, setDeleteApp] = usePostHodor<string, { value: boolean }>("/apps", { method: "delete" })
  const getApps = useFetchHodor<AppResp[]>("/apps")

  const [newAppName, setNewAppName] = useState("")

  const createClick = () => {
    setCreateApp({ name: newAppName })
  }

  const deleteClick = (app: AppResp) => {
    setDeleteApp(app.app.uuid)
  }

  /*console.info("apps render",
    { error, isLoading, response },
    { createError, createIsLoading, createResponse }
  )*/

  return (
    <Card>
      <FormGroup label={<H5>Create application</H5>} disabled={getApps.isLoading}>
        <ControlGroup>
          <InputGroup value={newAppName} onChange={e => setNewAppName(e.target.value)} disabled={getApps.isLoading}/>
          <Button intent={"success"} icon={"plus"} disabled={getApps.isLoading} onClick={createClick}>Add</Button>
        </ControlGroup>
      </FormGroup>

      {
        !!getApps.response &&
        <>
          <FormGroup>
            <hr/>
          </FormGroup>
          <FormGroup
            label={<H5>Applications</H5>}>
            {getApps.response.length === 0 && <NonIdealState title={"No apps created yet"}/>}
            {getApps.response.map(app => <>
                <ControlGroup key={app.app.uuid}>
                  <Button key={app.app.id} minimal fill alignText={"left"} icon={"application"} intent={"primary"}>
                    {app.app.name}
                  </Button>
                  <Button minimal icon={"cross"} intent={"danger"}
                          onClick={(e: React.MouseEvent<HTMLElement> | React.MouseEvent<HTMLButtonElement>) => {
                            deleteClick(app)
                            e.stopPropagation()
                            e.preventDefault()
                          }}/>
                </ControlGroup>
                <Card style={{ marginLeft: "2rem", marginRight: "2rem" }}>
                  <i>{app.clients[0].type}: {app.clients[0].uuid}</i>
                </Card>
              </>
            )}
          </FormGroup>
        </>
      }
    </Card>
  )
}

export default Apps
