import React, { useState } from "react"
import { Button, Card, Colors, FormGroup, H3, H6, InputGroup } from "@blueprintjs/core"
import { Link, useNavigate } from "react-router-dom"
import useHodor from "./hodor"


const HodorSignUp = () => {
  const navigate = useNavigate()
  const { props, signUp } = useHodor()

  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<string>()

  const clickSignUp = async (e: any) => {
    e.preventDefault()
    setLoading(true)
    setError(undefined)

    const req = {
      client: props.clientId,
      username,
      password
    }

    const resp = await signUp(req)

    console.info("register resp", resp)
    if (resp.error)
      setError(resp.error)
    else
      navigate("/")

    setLoading(false)
  }

  return (
    <Card style={{ maxWidth: "30rem" }} elevation={2}>
      <form onSubmit={clickSignUp}>
        <H3>Sign Up</H3>
        <FormGroup label={"Username"}>
          <InputGroup leftIcon={"user"} value={username} onChange={e => setUsername(e.target.value)}/>
        </FormGroup>
        <FormGroup label={"Password"}>
          <InputGroup type={"password"} leftIcon={"lock"} value={password} onChange={e => setPassword(e.target.value)}/>
        </FormGroup>

        {!!error && <H6 style={{ color: Colors.RED2 }}>Error: {error}</H6>}

        <FormGroup>
          <Button type={"submit"} intent={"primary"} text={"Sign Up"} fill loading={loading}/>
        </FormGroup>

        <FormGroup>
          <hr/>
        </FormGroup>

        <H6 style={{ textAlign: "center" }}>Already have account? <Link to={"/login"}>Login</Link></H6>
      </form>
    </Card>
  )
}

export default HodorSignUp
