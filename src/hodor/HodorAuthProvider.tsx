import React from "react"
import "./HodorAuth.sass"
import { HodorContext, HodorProps, UserAuth, UsernamePassword } from "./hodor"
import postHodor from "./postHodor"
import useLocalStorage from "./useLocalStorage"


export interface HodorAuthProviderOptions extends HodorProps {
  children?: React.ReactNode
}

const key = "hodor-user"

const HodorAuthProvider = (opts: HodorAuthProviderOptions) => {
  const [user, setUser] = useLocalStorage<UserAuth | undefined>(key)

  const login = (username: UsernamePassword) => {
    return postHodor(opts, "/login", username).then(user => {
      setUser(user)
      return user
    })
  }

  const signUp = (username: UsernamePassword) => {
    return postHodor(opts, "/register", username).then(user => {
      setUser(user)
      return user
    })
  }

  const logout = () => setUser(undefined)

  return (
    <HodorContext.Provider value={{
      props: opts,
      login,
      signUp,
      logout,
      isAuthenticated: !!user,
      user
    }}>
      {opts.children}
    </HodorContext.Provider>
  )
}

export default HodorAuthProvider
