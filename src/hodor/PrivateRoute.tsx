import useHodor from "./hodor"
import { Navigate, Route } from "react-router-dom"
import React from "react"
import { RouteProps } from "react-router"

const PrivateRoute = ({ children, ...rest }: { children: React.ReactElement, rest: RouteProps }) => {
  const { isAuthenticated } = useHodor()

  return (
    <Route
      {...rest}
      element={isAuthenticated
        ? children
        : <Navigate to={"/login"} />
      }/>
  )
}


export default PrivateRoute
