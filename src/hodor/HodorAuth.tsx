import React from "react"
import { Route, Routes } from "react-router-dom"
import HodorLogin from "./HodorLogin"
import HodorSignUp from "./HodorSignUp"
import PrivateRoute from "./PrivateRoute"

const HodorAuth = ({children}: {children: React.ReactElement}) => {
  return (
    <div className={"auth"}>
      <Routes>
        <Route path={"/login"}>
          <HodorLogin/>
        </Route>
        <Route path={"/signup"}>
          <HodorSignUp/>
        </Route>
        <PrivateRoute rest={{path: "/"}}>
          {children}
        </PrivateRoute>
      </Routes>
    </div>
  )
}

export default HodorAuth
