import { createContext, useContext } from "react"


export interface BaseResponse {
  error?: string
}

export interface Me {
  uuid: string
  username: string
  data: string
}

export interface UserAuth extends BaseResponse {
  accessToken: string
  me: Me
}

export interface UsernamePassword {
  client: string
  username: string
  password: string
}

export interface HodorProps {
  host: string
  clientId: string
}

export interface HodorContext {
  props: HodorProps
  login: (username: UsernamePassword) => Promise<UserAuth>
  signUp: (username: UsernamePassword) => Promise<UserAuth>
  logout: () => void,
  isAuthenticated: boolean
  user?: UserAuth
}

const stub = (): never => {
  throw new Error('You forgot to wrap your component in <HodorProvider>.');
};

const initialContext: HodorContext = {
  props: {
    clientId: "",
    host: ""
  },
  login: stub,
  signUp: stub,
  logout: stub,
  isAuthenticated: false,
  user: undefined
}

export const HodorContext = createContext(initialContext)

const useHodor = (): HodorContext =>
  useContext(HodorContext) as HodorContext

export default useHodor;
