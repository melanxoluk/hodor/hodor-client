import { HodorProps } from "./hodor"

export default async function postHodor(props: HodorProps, url: string, body: object) {
  const rawResp = await fetch(
    props.host + url,
    {
      method: "post",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })

  const text = await rawResp.text() ?? "{}"

  return JSON.parse(text)
}
