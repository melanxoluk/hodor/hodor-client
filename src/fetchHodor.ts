import { useEffect, useState } from "react"
import useHodor from "./hodor/hodor"


const useFetchHodor = <T>(url: string, body?: object, options?: RequestInit) => {
  const { user } = useHodor()

  const [isLoading, setIsLoading] = useState(false)
  const [response, setResponse] = useState<T | undefined>(undefined)
  const [error, setError] = useState(null)

  useEffect(() => {
    console.info("use fetch header run effect", url, options)

    const fetchData = async () => {
      setIsLoading(true)

      try {
        const res = await fetch(
          "https://hodor.melanxoluk.ru/api/v1" + url,
          {
            ...options,
            headers: {
              "Content-Type": "application/json",
              "Authorization": user?.accessToken ?? ""
            },
            body: JSON.stringify(body)
          })
        const json = await res.json()
        setResponse(json)
        setIsLoading(false)
      } catch (error) {
        setError(error)
      }
    }

    fetchData()
  }, [user, url, body, options])

  return { response, error, isLoading }
}

export const usePostHodor = <Req, Resp>(url: string, options?: RequestInit): [any, (req: Req) => void] => {
  const { user } = useHodor()

  const [resp, setResp] = useState({ isLoading: false, response: undefined, error: undefined })
  const [body, setBody] = useState<Req | undefined>(undefined)

  useEffect(() => {
    console.info("use fetch header run effect", url, options)

    const fetchData = async () => {
      setResp({ isLoading: true, response: undefined, error: undefined })

      try {
        const res = await fetch(
          "https://hodor.melanxoluk.ru/api/v1" + url,
          {
            method: "post",
            headers: {
              "Content-Type": "application/json",
              "Authorization": user?.accessToken ?? ""
            },
            body: JSON.stringify(body),
            ...options
          })
        const json = await res.json()
        setResp({ isLoading: false, response: json, error: undefined })
      } catch (error) {
        setResp({ isLoading: false, response: undefined, error: error })
      }
    }

    if (!resp.isLoading && !resp.response && body) {
      console.info("use fetch header run effect call", { url, options, resp, body })
      fetchData()
    }

  }, [url, options, body, resp, user])

  return [resp, (newBody: Req) => {
    setResp({ isLoading: false, response: undefined, error: undefined })
    setBody(newBody)
  }]
}

export default useFetchHodor
