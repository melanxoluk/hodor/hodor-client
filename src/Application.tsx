import React from "react"
import HodorAuth from "./hodor/HodorAuth"
import { Button } from "@blueprintjs/core"
import useHodor from "./hodor/hodor"
import { Routes, Route, Navigate } from "react-router-dom"
import Apps from "./Apps"

const Application = () => {
  const { user, logout } = useHodor()

  return (
    <HodorAuth>
      <Routes>
        <Route path={"/apps"}>
          <Apps/>
        </Route>
        <Route path={"/"}>
          <Navigate to={"/apps"}/>
        </Route>

        <div>I am logined: {user?.me?.username}</div>
        <Button onClick={logout}>Logout</Button>
      </Routes>
    </HodorAuth>
  )
}

export default Application
