import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import * as serviceWorker from "./serviceWorker"
import "normalize.css"
import "@blueprintjs/core/lib/css/blueprint.css"
import "@blueprintjs/select/lib/css/blueprint-select.css"
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css"
import "@blueprintjs/icons/lib/css/blueprint-icons.css"
import { FocusStyleManager } from "@blueprintjs/core"
import Application from "./Application"
import { BrowserRouter } from "react-router-dom"
import HodorAuthProvider from "./hodor/HodorAuthProvider"

FocusStyleManager.onlyShowFocusOnTabs()

ReactDOM.render(
  <BrowserRouter>
    <HodorAuthProvider
      host={"https://hodor.melanxoluk.ru/api/v1"}
      clientId={"fd1c662f-b196-43a6-a914-368458c1bb83"}>
      <Application/>
    </HodorAuthProvider>
  </BrowserRouter>,
  document.getElementById("root"))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
